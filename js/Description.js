class Description {
    constructor() {}

    #main = 'This application will help you keep track of your utility meters. Indicators are supported for water, electricity and gas. If you are using the application for the first time, click the "New" button to enter the initial values. In the future, by clicking this button, you will be taken to a section where you can enter new current values. If you have previously entered some values, by clicking the "Data" button, you will see all the entered data in the form of a table. By pressing the "Graphs" button, you will display the entered data in the form of graphs and this is convenient for visual perception. Also, be careful, pressing the red "Clear" button will delete all previously entered data.';

    #newForm = 'In this section, you can enter current values for today\'s date. Please note that the current values can be equal, but cannot be less than the previous ones. If so, you will receive an error message. Enter new values and click the "Confirm" button.';

    #table = 'In this section, the entered data is presented in the form of a table. You can use the "Edit" link to correct the latest values if they are wrong. But only the last ones. If the table has only a row with initial values, please use the "Clear" button to edit the values. This will clear the storage and you will be able to enter the initial values again.';

    #editForm = 'Now you can edit the last entered values. Enter the correct values and click the "Confirm" button. You still won\'t be able to enter values that are less than the previous ones.';
    
    #graphs = 'The points of the graphs mean the difference between the values of adjacent periods. This allows you to visualize the changes in values.';

    getMain() {
        return this.#main;
    }

    getNewForm() {
        return this.#newForm;
    }

    getEditForm() {
        return this.#editForm;
    }

    getTable() {
        return this.#table;
    }

    getGraphs() {
        return this.#graphs;
    }
    
}
