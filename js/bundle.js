window.onload = () => {

    const input = new Date(); //was "let"
    let date = String(input.getDate()).padStart(2, '0') + '/' + String(input.getMonth() + 1).padStart(2, '0') + '/' + input.getFullYear();

    const dateField = document.getElementById('date');
    dateField.innerText = date;

    //const main = document.getElementsByTagName('main')[0];
    const main = document.querySelector('main');
    main.style.visibility = 'visible';

    const newBtn = document.querySelectorAll('button')[0];
    const dataBtn = document.querySelectorAll('button')[1];
    const graphBtn = document.querySelectorAll('button')[2];
    const clearBtn = document.querySelectorAll('button')[3];
    const hideBtn = document.getElementById('hideBtn');
    const saveValues = document.getElementById('saveValues');
    const formMessage = document.getElementById('formMessage');
    const table = document.querySelector('tbody');

    const instruction = document.getElementById('instruction');
    const desc = document.getElementById('description');
    const descText = new Description();
    desc.innerText = descText.getMain();

    const graphs = document.getElementById('graphs');
    graphs.style.display = 'none';
    const inputForm = document.getElementById('inputForm');
    inputForm.style.display = 'none';
    const dataTable = document.getElementById('dataTable');
    dataTable.style.display = 'none';

    let editFlag = false;

    try {
        const storage = JSON.parse(localStorage.storage);
        const minCold = storage[storage.length - 1].cold;
        const minHot = storage[storage.length - 1].hot;
        const minGas = storage[storage.length - 1].gas;
        const minPower = storage[storage.length - 1].power;

        document.getElementById('inputColdWater').setAttribute('min', minCold);
        document.getElementById('inputColdWater').value = minCold;
        document.getElementById('inputHotWater').setAttribute('min', minHot);
        document.getElementById('inputHotWater').value = minHot;
        document.getElementById('inputGas').setAttribute('min', minGas);
        document.getElementById('inputGas').value = minGas;
        document.getElementById('inputElectricity').setAttribute('min', minPower);
        document.getElementById('inputElectricity').value = minPower;
    } catch (ex) {
        console.warn(ex);
    };

    saveValues.addEventListener('click', () => {
        let isStorageEmpty = true;
        let storage = new Array();
        try {
            storage = JSON.parse(localStorage.storage);
            isStorageEmpty = false;
        } catch { isStorageEmpty = true };
        if (isStorageEmpty) {
            storage[0] = getFormValues();
            localStorage.storage = JSON.stringify(storage);
            formMessage.style.color = 'green';
            formMessage.innerHTML = '';
            formMessage.innerHTML = '<h3>Saved successfully!</h3>';
        } else {
            const current = getFormValues();
            if (editFlag) {
                storage = updateStorage(current, storage[storage.length - 2], storage);
                localStorage.storage = JSON.stringify(storage);
            } else {
                storage = updateStorage(current, storage[storage.length - 1], storage);
                localStorage.storage = JSON.stringify(storage);
            }
        }
    })

    function getFormValues() {
        const cold = document.getElementById('inputColdWater').value;
        const hot = document.getElementById('inputHotWater').value;
        const gas = document.getElementById('inputGas').value;
        const power = document.getElementById('inputElectricity').value;
        return {
            date: date,
            cold: cold,
            hot: hot,
            gas: gas,
            power: power
        };
    }

    function updateStorage(current, previous, storage) {
        const warn1 = document.createElement('div');
        warn1.className = 'text-start';
        warn1.innerHTML = 'Caution!'
        const warn2 = document.createElement('div');
        warn2.className = 'text-start';
        warn2.innerHTML = 'Current value less than previous.'
        const warn3 = document.createElement('div');
        warn3.className = 'text-start';
        const warn4 = document.createElement('div');
        warn4.className = 'text-start';
        const warn5 = document.createElement('div');
        warn5.className = 'text-start';
        formMessage.style.color = 'red';

        const cc = parseInt(current.cold);
        const pc = parseInt(previous.cold);
        const ch = parseInt(current.hot);
        const ph = parseInt(previous.hot);
        const cg = parseInt(current.gas);
        const pg = parseInt(previous.gas);
        const cp = parseInt(current.power);
        const pp = parseInt(previous.power);

        if (cc < pc) {
            warn3.innerHTML = 'Field: Cold Water';
            warn4.innerHTML = `Previous: ${previous.cold}`;
            warn5.innerHTML = `Current: ${current.cold}`;
            formMessage.innerHTML = '';
            formMessage.append(warn1);
            formMessage.append(warn2);
            formMessage.append(warn3);
            formMessage.append(warn4);
            formMessage.append(warn5);
        } else {
            if (ch < ph) {
                warn3.innerHTML = 'Field: Hot Water';
                warn4.innerHTML = `Previous: ${previous.hot}`;
                warn5.innerHTML = `Current: ${current.hot}`;
                formMessage.innerHTML = '';
                formMessage.append(warn1);
                formMessage.append(warn2);
                formMessage.append(warn3);
                formMessage.append(warn4);
                formMessage.append(warn5);
            } else {
                if (cg < pg) {
                    warn3.innerHTML = 'Field: Gas';
                    warn4.innerHTML = `Previous: ${previous.gas}`;
                    warn5.innerHTML = `Current: ${current.gas}`;
                    formMessage.innerHTML = '';
                    formMessage.append(warn1);
                    formMessage.append(warn2);
                    formMessage.append(warn3);
                    formMessage.append(warn4);
                    formMessage.append(warn5);
                } else {
                    if (cp < pp) {
                        warn3.innerHTML = 'Field: Electricity';
                        warn4.innerHTML = `Previous: ${previous.power}`;
                        warn5.innerHTML = `Current: ${current.power}`;
                        formMessage.innerHTML = '';
                        formMessage.append(warn1);
                        formMessage.append(warn2);
                        formMessage.append(warn3);
                        formMessage.append(warn4);
                        formMessage.append(warn5);
                    } else {
                        if (current.date === previous.date) {
                            storage[storage.length - 1] = current;
                            formMessage.style.color = 'green';
                            formMessage.innerHTML = '';
                            formMessage.innerHTML = '<h3>Updated successfully!</h3>';
                        } else {
                            if (!editFlag) {
                                storage[storage.length] = current;
                                formMessage.style.color = 'green';
                                formMessage.innerHTML = '';
                                formMessage.innerHTML = '<h3>Saved successfully!</h3>';
                            } else {
                                current.date = storage[storage.length - 1].date;
                                storage[storage.length - 1] = current;
                                formMessage.style.color = 'green';
                                formMessage.innerHTML = '';
                                formMessage.innerHTML = '<h3>Edited successfully!</h3>';
                            }
                        }
                    }
                }
            }
        }
        return storage;
    }

    function deployTable() {
        let isStorageEmpty = true;
        let storage = new Array();
        try {
            storage = JSON.parse(localStorage.storage);
            isStorageEmpty = false;
        } catch { isStorageEmpty = true };
        if (isStorageEmpty) {
            alert('Storage is empty. Use "New" button for adding data.')
        } else {
            table.innerHTML = "";
            if (storage.length >= 2) {
                for (let i = 0; i < storage.length - 1; i++) {
                    let row = document.createElement('tr');
                    row.innerHTML = `<td>${storage[i].date}</td><td>${storage[i].cold}</td><td>${storage[i].hot}</td><td>${storage[i].gas}</td><td>${storage[i].power}</td><td></td>`;
                    table.append(row);
                }
                let l = storage.length - 1;
                let row = document.createElement('tr');
                row.innerHTML = `<td>${storage[l].date}</td><td>${storage[l].cold}</td><td>${storage[l].hot}</td><td>${storage[l].gas}</td><td>${storage[l].power}</td><td id="editLink">Edit</td>`;
                table.append(row);
                let editLink = document.getElementById("editLink");
                editLink.addEventListener('click', () => {
                    desc.innerText = descText.getEditForm();
                    editFlag = true;
                    inputForm.style.display = 'block';
                    dataTable.style.display = 'none';
                    formMessage.innerHTML = '';
                })
            } else {
                let l = storage.length - 1;
                let row = document.createElement('tr');
                row.innerHTML = `<td>${storage[l].date}</td><td>${storage[l].cold}</td><td>${storage[l].hot}</td><td>${storage[l].gas}</td><td>${storage[l].power}</td><td></td>`;
                table.append(row);
            }
        }
    }

    function deployGraphs() {
        const labels = new Array();
        let isStorageEmpty = true;
        let storage = new Array();
        try {
            storage = JSON.parse(localStorage.storage);
            isStorageEmpty = false;
        } catch { isStorageEmpty = true };
        if (isStorageEmpty) {
            alert('Storage is empty. Use "New" button for adding data.')
        } else {
            if (storage.length == 1) graphs.style.display = 'none';
            for (let i = 0; i < storage.length; i++) {
                labels[i] = storage[i].date;
            }
            //Cold water graph
            let valuesCold = new Array();

            for (let i = 1; i < storage.length; i++) {
                valuesCold[i] = storage[i].cold - storage[i - 1].cold;
            }
            const dataCold = {
                labels: labels,
                datasets: [{
                    label: 'Cold Water',
                    backgroundColor: 'rgb(22,210,240)',
                    borderColor: 'rgb(22,210,240)',
                    data: valuesCold,
                }]
            };
            const configCold = {
                type: 'line',
                data: dataCold,
                options: {
                    borderWidth: 3,
                    plugins: {
                        legend: {
                            onClick: null,
                            labels: {
                                font: {
                                    size: 20,
                                }
                            }
                        }
                    }
                }
            };
            const coldChart = new Chart(
                document.getElementById('coldChart'),
                configCold
            );
            //Hotwater graph
            let valuesHot = new Array();

            for (let i = 1; i < storage.length; i++) {
                valuesHot[i] = storage[i].hot - storage[i - 1].hot;
            }
            const dataHot = {
                labels: labels,
                datasets: [{
                    label: 'Hot Water',
                    backgroundColor: 'rgb(240,80,80)',
                    borderColor: 'rgb(240,80,80)',
                    data: valuesHot,
                }]
            };
            const configHot = {
                type: 'line',
                data: dataHot,
                options: {
                    borderWidth: 3,
                    plugins: {
                        legend: {
                            onClick: null,
                            labels: {
                                font: {
                                    size: 20
                                }
                            }
                        }
                    }
                }
            };
            const hotChart = new Chart(
                document.getElementById('hotChart'),
                configHot
            );
            //Gas graph
            let valuesGas = new Array();

            for (let i = 1; i < storage.length; i++) {
                valuesGas[i] = storage[i].gas - storage[i - 1].gas;
            }
            const dataGas = {
                labels: labels,
                datasets: [{
                    label: 'Gas',
                    backgroundColor: 'rgb(254,251,2)',
                    borderColor: 'rgb(254,251,2)',
                    data: valuesGas,
                }]
            };
            const configGas = {
                type: 'line',
                data: dataGas,
                options: {
                    borderWidth: 3,
                    plugins: {
                        legend: {
                            onClick: null,
                            labels: {
                                font: {
                                    size: 20
                                }
                            }
                        }
                    }
                }
            };
            const gasChart = new Chart(
                document.getElementById('gasChart'),
                configGas
            );
            //Power graph
            let valuesPower = new Array();

            for (let i = 1; i < storage.length; i++) {
                valuesPower[i] = storage[i].power - storage[i - 1].power;
            }
            const dataPower = {
                labels: labels,
                datasets: [{
                    label: 'Electricity',
                    backgroundColor: 'rgb(86,240,80)',
                    borderColor: 'rgb(86,240,80)',
                    data: valuesPower,
                }]
            };
            const configPower = {
                type: 'line',
                data: dataPower,
                options: {
                    borderWidth: 3,
                    plugins: {
                        legend: {
                            onClick: null,
                            labels: {
                                font: {
                                    size: 20
                                }
                            }
                        }
                    }
                }
            };
            const powerChart = new Chart(
                document.getElementById('powerChart'),
                configPower
            );
        }
    }

    clearBtn.addEventListener('click', () => {
        let warning = confirm('Are you sure? This action will destroy all stored data!');
        if (warning) {
            localStorage.clear();
            alert('All data removed! Use "New" button for adding data.');
            window.location.reload();
        }
    })

    newBtn.addEventListener('click', () => {
        desc.innerText = descText.getNewForm();
        dataTable.style.display = 'none';
        graphs.style.display = 'none';
        inputForm.style.display = 'block';
        editFlag = false;
        formMessage.innerHTML = '';
    })

    dataBtn.addEventListener('click', () => {
        desc.innerText = descText.getTable();
        inputForm.style.display = 'none';
        graphs.style.display = 'none';
        dataTable.style.display = 'block';
        deployTable();

    })

    graphBtn.addEventListener('click', () => {
        desc.innerText = descText.getGraphs();
        inputForm.style.display = 'none';
        dataTable.style.display = 'none';
        graphs.style.display = 'block';
        deployGraphs();

    })

    hideBtn.addEventListener('click', () => {
        instruction.style.display = 'none';
    })

}